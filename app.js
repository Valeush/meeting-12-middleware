let express = require('express');
let app = express();

let personas = [
    {id: 1, nombre: "Pepe", email: "pepe@nada.com"},
    {id: 2, nombre: "Hugo", email: "hugo@nada.com"},
    {id: 3, nombre: "Juan", email: "juan@nada.com"}
]

function captura_url (req, res, next) {
   console.log(req.url);
   next(); 
}

app.use(captura_url);

//devuelve la version
app.get('/version', function (req, res){
    res.send('v 2.0.0');
})

//devuelve todas las presonas
app.get('/personas', function (req, res){
    console.log(personas);
    res.send(personas);
})

//devuelve persona por id

app.get('/personas/:id', function (req, res){
    let id = req.params.id;
    let persona_buscada = personas.find(persona => persona.id == id);
    if(persona_buscada){
    console.log(persona_buscada);
    res.send(persona_buscada);
    }else{
    res.status(400).json("Dato no encontrado");
    } 
})

//devuelve persona por nombre

app.get('/personas/nombre/:nombre', function (req, res){
    let nombre = req.params.nombre;
    let persona_buscada = personas.find(persona => persona.nombre == nombre);
   if (persona_buscada){
    console.log(persona_buscada);
    res.send(persona_buscada);  
   }else{
       res.status(400).json("Dato no encontrado");
   }    
})


app.listen(3000, function(){
    console.log('escuchando el puerto 3000');
})